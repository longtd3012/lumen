<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/** @var TYPE_NAME $router */
$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'user'], function () use ($router) {
    $router->get('/' , 'UserController@list');

    $router->get("/{id:[0-9]+}", "UserController@view");

    $router->post('/create', 'UserController@create');

    $router->get('/delete/{id:[0-9]+ }', 'UserController@delete');

});

