<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call('UsersTableSeeder');
        \App\User::insert([
            ['name' => 'Longtd', 'email' => 'longtd@funtap.vn', 'password' => '123456'],
            ['name' => 'Sontt', 'email' => 'sontt@funtap.vn', 'password' => '123456']
        ]);
    }
}
