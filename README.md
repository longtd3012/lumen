# Lumen PHP Framework
Required OS
- PHP > 7.2
- Apache/2.4.*
- Mysql > 5.*

Before run
- Create database : laravel

Run project
- php artisan db:seed
- php -S localhost:8000 -t public

API
- List users : http://localhost:8000/user
- Find user by ID : http://localhost:8000/user/id
- Create new user (method POST) : http://localhost:8000/user/create
- Delete user by ID : http://localhost:8000/user/delete/id

## refer
- [lumen.laravel.com](https://lumen.laravel.com/)
- [viblo.asia](https://viblo.asia/)
- [stackoverflow.com](https://stackoverflow.com/)

## License

The Lumen framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
