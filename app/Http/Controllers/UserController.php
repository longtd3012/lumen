<?php


namespace App\Http\Controllers;


use App\Service\UserInterface;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public $userService;
    public function __construct(UserInterface $userService)
    {
        $this->userService = $userService;
    }

    public function list(){
        $list = $this->userService->list();
        return response()->json($list);
    }

    public function view($id){
        $user = $this->userService->view($id);
        return response()->json($user);
    }

    public function delete($id){
        return $this->userService->delete($id);
    }

    public function create(Request $request){
        return $this->userService->create($request);
    }
}
