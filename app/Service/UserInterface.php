<?php


namespace App\Service;


interface UserInterface
{
    public function list();
    public function view($id);
    public function delete($id);
    public function create($req);
}
