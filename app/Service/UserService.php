<?php


namespace App\Service;


use App\User;

class UserService implements UserInterface
{

    public function list()
    {
        // TODO: Implement list() method.
        return User::all();
    }

    public function view($id)
    {
        // TODO: Implement get() method.
        return User::find($id);
    }

    public function delete($id)
    {
        // TODO: Implement delete() method.
        return User::findOrFail($id)->delete();
    }

    public function create($req)
    {
        // TODO: Implement create() method.
//        dd($req->all());
        return User::create($req->all());
    }
}
